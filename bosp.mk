
ifdef CONFIG_CONTRIB_CPUBOUND

# Targets provided by this project
.PHONY: cpubound clean_cpubound

# Add this to the "contrib_testing" target
testing: cpubound
clean_testing: clean_cpubound

MODULE_CONTRIB_USER_CPUBOUND=contrib/user/Cpubound

cpubound: external
	@echo
	@echo "==== Building Cpubound ($(BUILD_TYPE)) ===="
	@[ -d $(MODULE_CONTRIB_USER_CPUBOUND)/build/$(BUILD_TYPE) ] || \
		mkdir -p $(MODULE_CONTRIB_USER_CPUBOUND)/build/$(BUILD_TYPE) || \
		exit 1
	@cd $(MODULE_CONTRIB_USER_CPUBOUND)/build/$(BUILD_TYPE) && \
		CXX=$(CXX) CFLAGS="--sysroot=$(PLATFORM_SYSROOT)" \
		cmake $(CMAKE_COMMON_OPTIONS) || \
		exit 1
	@cd $(MODULE_CONTRIB_USER_CPUBOUND)/build/$(BUILD_TYPE) && \
		make -j$(CPUS) install || \
		exit 1

clean_cpubound:
	@echo
	@echo "==== Clean-up Cpubound Application ===="
	@[ ! -f $(BUILD_DIR)/usr/bin/cpubound ] || \
		rm -f $(BUILD_DIR)/etc/bbque/recipes/Cpubound*; \
		rm -f $(BUILD_DIR)/usr/bin/cpubound*
	@rm -rf $(MODULE_CONTRIB_USER_CPUBOUND)/build
	@echo

else # CONFIG_CONTRIB_CPUBOUND

cpubound:
	$(warning contib module Cpubound disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_CONTRIB_CPUBOUND

