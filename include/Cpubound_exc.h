/**
 *       @file  Cpubound_exc.h
 *      @brief  The Cpubound BarbequeRTRM application
 *
 * Description: to be done...
 *
 *     @author  Name Surname (nickname), your@email.com
 *
 *     Company  Politecnico di Milano
 *   Copyright  Copyright (c) 20XX, Name Surname
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#ifndef CPUBOUND_EXC_H_
#define CPUBOUND_EXC_H_

#include <bbque/bbque_exc.h>
#include <vector>
#include <iostream>
#include <mutex>
#include <sys/prctl.h>

using bbque::rtlib::BbqueEXC;

/**
 * @class Semaphore
 *
 * Exploited for thread syncronization.
 */
class Semaphore {
	private:
		std::mutex sem_mtx;
		std::condition_variable sem_cv;
		int value = 0;
	public:
		Semaphore(){}
		/** Setting the semaphore value. If a thread waits on the semaphore,
		 * it will wait until this value reaches 0 */
		void set(int count){
			value = count;
		}
		/** Decreasing the semaphore value. If it reaches 0, the waiting
		 * threads are notified
		 */
		void signal(int count = 1){
			std::unique_lock<std::mutex> lock(sem_mtx);
			value -= count;
			if (value != 0)
				return;
			lock.unlock();
			sem_cv.notify_all();
		}
		/** Waiting on the semaphore. If the value if greater than 0, the thread
		 * will wait until the value reaches 0
		 */
		void wait(){
			std::unique_lock<std::mutex> lock(sem_mtx);
			if (value > 0)
				sem_cv.wait(lock);
		}
};

/**
 * @class Cpubound
 *
 *  Cpubound is an application able to create memory noise in the cache
 *  levels. The application reads and writes to a randomized big chunk of
 *  memory.
 */
class Cpubound : public BbqueEXC {

	/**
	 * Jobs number
	 */
	int jn;
	/**
	 * Exploited to force the threads number, if needed.
	 */
	uint8_t thread_number;
	/**
	 * Number of already performed execution cycles.
	 */
	int performed_cycles = 0;
	/**
	 * Number of already performed jobs.
	 */
	int performed_jobs = 0;

	/* Inter-threads syncronization */
	bool finished = false;
	Semaphore sem_f, sem_s;
	std::vector<std::condition_variable *> cv_pool;
	std::mutex cv_mtx;

	/* Maximum and current parallelism */
	uint8_t parallelism;
	uint8_t threshold = 0;
	std::vector<std::thread> trd_pool;

	int operations;

public:

	/**
	 * @brief Constructor
	 */
	Cpubound(std::string const & name,
			int jobs_number,
			uint8_t force_trd_number,
			std::string const & recipe,
			RTLIB_Services_t *rtlib,
			uint8_t pes = ( 3 * std::thread::hardware_concurrency() / 4));

	/**
	 * @brief Activates a thread
	 */
	void Run(uint8_t);
	/**
	 * @brief Workload of an activated thread
	 */
	void SingleThread(uint8_t);
	void NotifyResult(int);
	int GetQuery();

private:

	/**
	 * @brief RTLIB: Initialization
	 */
	RTLIB_ExitCode_t onSetup();
	/**
	 * @brief RTLIB: Configuration, according to the chosen AWM
	 */
	RTLIB_ExitCode_t onConfigure(uint8_t awm_id);
	/**
	 * @brief RTLIB: single execution cycle
	 */
	RTLIB_ExitCode_t onRun();
	/**
	 * @brief RTLIB: monitoring, after a single execution cycle
	 */
	RTLIB_ExitCode_t onMonitor();
	/**
	 * @brief RTLIB: releasing
	 */
	RTLIB_ExitCode_t onRelease();

	int total_queries = 0;
};

#endif // CPUBOUND_EXC_H_
