/**
 *       @file  Cpubound_exc.cc
 *      @brief  The Cpubound BarbequeRTRM application
 *
 * Description: A CPU-bound, multithreaded application
 *
 *     @author  Simone Libutti, simone.libutti@polimi.it
 *
 *     Company  Politecnico di Milano
 *   Copyright  Copyright (c) 2014, Simone Libutti
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */


#include "Cpubound_exc.h"

#include <cstdio>
#include <bbque/utils/utility.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdint.h>
#include <inttypes.h>
#include <fstream>
#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <math.h>

// Setup logging
#undef  BBQUE_LOG_MODULE
#define BBQUE_LOG_MODULE "aem.Cpubound"
#undef  BBQUE_LOG_UID
#define BBQUE_LOG_UID GetChUid()

#define BASE 1000000000
#define RAND 100000000

#include <bbque/monitors/operating_point.h>
namespace ba = bbque::rtlib::as;
extern ba::OperatingPointsList opList;

Cpubound::Cpubound(std::string const & name,
		int jobs_number,
		uint8_t force_trd_number,
		std::string const & recipe,
		RTLIB_Services_t *rtlib,
		uint8_t pes) :
	BbqueEXC(name, recipe, rtlib),
	parallelism(pes) {

	jn = jobs_number;
	thread_number = force_trd_number;

	if (thread_number > parallelism)
		parallelism = thread_number;

	fprintf(stderr, FW("New Cpubound::Cpubound()\n"));
	fprintf(stderr, FI("Running with parameters %d, %" PRIu8 " \n"),
			jn, thread_number);

	if (thread_number > 0) {
		fprintf(stderr, FI("Forcing threads number to %d\n"), thread_number);
		parallelism = thread_number;
	}

	fprintf(stderr, FI("EXC Unique IDentifier (UID): %u\n"), GetUid());

}

RTLIB_ExitCode_t Cpubound::onRelease(){

	finished = true;

	for(auto &cv : cv_pool)
		cv->notify_one();

	fprintf(stderr, FI("Waiting threads to terminate...\n"));
	for(auto &trd : trd_pool)
		trd.join();

	return RTLIB_OK;

}

RTLIB_ExitCode_t Cpubound::onSetup() {

	if(thread_number > 0)
		parallelism = thread_number;

	/* Resizing the threadpool. 'parallelism' is three quarters the
	 * number of PEs featured by the system. Usually, that is the number of
	 * PEs exploited as Managed Device */
	trd_pool.resize(parallelism);
	/* Creating a vector of condition variables, one per thread. In this way,
	 * I can put a certain number of threads into sleep and wake them only
	 * if they are needed, without overheads. Making all the threads wait on
	 * the same condition variable would cause overheads because, even on a
	 * notify_one() call, all the threads wake for a short period of time.
	 * Even in the case where the application reconfigures with a notable
	 * frequence, the overhead reduction remains robust.
	 * */
	cv_pool.resize(parallelism);
	for (uint8_t i = 0; i < parallelism; ++i) {
		trd_pool[i] = std::thread(&Cpubound::Run, this, i);
		cv_pool[i] = new std::condition_variable();
	}

	operations = GetQuery();

	return RTLIB_OK;
}

RTLIB_ExitCode_t Cpubound::onConfigure(uint8_t awm_id) {

	uint8_t t = opList[awm_id].parameters["thread"];

	if ( thread_number == 0 ) {
		threshold = t;
		logger->Warn("Using %d threads", threshold);
	}
	else {
		threshold = parallelism;
		logger->Warn("OnConfigure: Threads number fixed to %d by user", threshold);
	}

	if (jn - performed_jobs < threshold) threshold = jn - performed_jobs;

	return RTLIB_OK;
}

RTLIB_ExitCode_t Cpubound::onRun() {

	/* I will need to wake and synchronize only 'threshold' threads */
	sem_s.set(threshold);
	sem_f.set(threshold);

	/* Waking up the threads */
	for (uint8_t i = 0; i < threshold; ++i){
		cv_pool[i]->notify_one();
		/* One less thread to wait before starting */
		sem_s.signal();
	}

	/* Waiting waking threads before starting */
	sem_f.wait();

	std::unique_lock<std::mutex> lck(cv_mtx);
	lck.unlock();

	return RTLIB_OK;
}

/**
 * @brief Activate a worker thread
 *
 * @param id is the id of the threadto be activated
 *
 * Once a thread is activated, it will run until the application terminates.
 * The thread waits on a condition variable so, each cycle, either it sleeps
 * or perform the workload.
 */
void Cpubound::Run(uint8_t id) {

	while (true) {
		std::unique_lock<std::mutex> lck(cv_mtx);

		/* Signal that the workload has been executed */
		sem_f.signal();

		/* Wait to be wakened */
		cv_pool[id]->wait(lck);
		lck.unlock();

		/* If there is no more work to do, terminate... */
		if(finished) {
			break;
		}

		/* ...else, wait all the threads to be ready */
		sem_s.wait();
		/* Execute the workload */
		SingleThread(id);
	}
}

/**
 * @brief Single cycle workload
 *
 * @param id is the id of the thread which executes the workload
 *
 * Each cycle, each thread either sleeps or performs this workload.
 */
void Cpubound::SingleThread(uint8_t id) {

#if 0

	int values[] = {GetQuery(), GetQuery()};
	int remaining = operations;

	int res = 0;
	int index=0;

	while ( remaining > 0 ) {

		res = values[index];

		remaining--;

		if ( index == 0 ) ++index;
		else --index;

	}

	NotifyResult(res);

#else

	asm ("b1:dec %0;"
		"jnz b1;"
	     :			/* output */
	     :"r"(operations)	/* input */
	     :			/* clobbered register */
	);
	NotifyResult(0);

#endif

}

RTLIB_ExitCode_t Cpubound::onMonitor() {


	performed_cycles = Cycles();

//	fprintf(stderr, FI("[onMonitor] Cycle %d : Processed Jobs %d to %d\n"),
//		performed_cycles, performed_jobs - threshold + 1, performed_jobs);

	if (performed_jobs >= jn && jn > 0)
		return RTLIB_EXC_WORKLOAD_NONE;

	if (jn - performed_jobs < threshold)
		fprintf(stderr, FI("[onMonitor] Exceeding performed jobs "\
		"limit to exploit %d idle threads\n"), threshold - jn + performed_jobs);

	return RTLIB_OK;
}

void Cpubound::NotifyResult(int result) {

	performed_jobs ++;

}

int Cpubound::GetQuery() {

//	return BASE + (rand() % RAND);
	return BASE;

}
